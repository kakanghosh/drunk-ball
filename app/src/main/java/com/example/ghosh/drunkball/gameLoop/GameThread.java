package com.example.ghosh.drunkball.gameLoop;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.text.TextPaint;
import android.util.Log;
import android.view.SurfaceHolder;

import com.example.ghosh.drunkball.R;
import com.example.ghosh.drunkball.animation.GameAnimation;
import com.example.ghosh.drunkball.animation.ObstacleAnimation;
import com.example.ghosh.drunkball.bitmapDesign.Ball;
import com.example.ghosh.drunkball.bitmapDesign.LifeBitmap;
import com.example.ghosh.drunkball.bitmapDesign.Obstacle;
import com.example.ghosh.drunkball.bitmapDesign.RestartBitmap;
import com.example.ghosh.drunkball.gameInterface.GameCollision;

import java.util.ArrayList;

/**
 * Created by GHOSH on 5/7/2017.
 */

public class GameThread extends Thread implements GameCollision {
    private final int MAX_FPS = 60;
    Context context;
    SurfaceHolder surfaceHolder;
    private int varying = 20;
    private boolean threadFlag;
    private Canvas canvas;
    private Bitmap ballBitmap;
    private Ball ball;
    // private Bitmap obstacleBitMap;
    private ArrayList<Bitmap> listOfObstableBitmap;
    // Obstacle obstacle;
    private int score;
    private RestartBitmap restartButton;
    private Bitmap restartBitmap;

    //Obtacle List
    private ArrayList<Obstacle> leftSideObstacle;
    private ArrayList<Obstacle> rightSideObstacle;


    private GameActivity activity;
    //High score shared Preference
    public static String HIGH_SCORE_PREFERENCE = "_HIGH_SCORE_PREF_";
    public static String HIGH_SCORE = "_HIGH_SCORE_";

    //Life Bitmap List
    private ArrayList<LifeBitmap> listOfLifeBitmap;
    private int lifeLine = 3;
    private Bitmap blackLife;
    private Bitmap redLife;
    private int indexOfLife = 0;


    public GameThread(SurfaceHolder holder, Context context, GameActivity activity) {

        super();
        this.context = context;
        this.activity = activity;
        this.surfaceHolder = holder;
        this.ball = null;

        this.ballBitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.ball2);
        this.ballBitmap = scaledBallBitmap(this.ballBitmap);


        //Initialize Array List
        this.listOfObstableBitmap = new ArrayList<>();
        this.leftSideObstacle = new ArrayList<>();
        this.rightSideObstacle = new ArrayList<>();

        //Adding bitmap in the possible obstac bitmap List
        this.listOfObstableBitmap.add(getScaledObastacleBitmap(R.drawable.obstacle));
        this.listOfObstableBitmap.add(getScaledObastacleBitmap(R.drawable.obstacle1));
        this.listOfObstableBitmap.add(getScaledObastacleBitmap(R.drawable.obstacle2));
        this.listOfObstableBitmap.add(getScaledObastacleBitmap(R.drawable.obstacle3));
        this.listOfObstableBitmap.add(getScaledObastacleBitmap(R.drawable.obstacle4));


        //Restart Bitmap

        this.restartBitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.restart);
        this.restartBitmap = scaledBitmap(restartBitmap);

        this.restartButton = new RestartBitmap(this.restartBitmap, GameSurface.getScreenWidth() / 2 - this.restartBitmap.getWidth() / 2,
                GameSurface.getScreenHeight() - this.restartBitmap.getHeight() * 2);



        //Life Bitmap List initialize
        lifeBitmapInitialize();
    }


    @Override
    public void run() {
        this.threadFlag = true;
        long startTime;
        long targetTime = 1000 / MAX_FPS;
        long waitingTime;
        long scoreTotalTime = 0;

        GameAnimation gameAnimation = new GameAnimation(this, this.activity);
        ObstacleAnimation obstacleAnimation = new ObstacleAnimation(this);

        gameAnimation.start();
        obstacleAnimation.start();



        while (this.threadFlag) {

            startTime = System.nanoTime();
            try {
                this.canvas = this.surfaceHolder.lockCanvas();
                synchronized (this.surfaceHolder) {
                    updateCanvas();
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (canvas != null) {
                    surfaceHolder.unlockCanvasAndPost(canvas);
                }
            }



            waitingTime = targetTime - ((System.nanoTime() - startTime) / 1000000);

            if (waitingTime > 0) {
                try {
                    this.sleep(waitingTime);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }

        gameAnimation.stopAnimationTthread();
        obstacleAnimation.stopObstacleAnimation();
       // scoreThread.stopScoreThread();


    }


    //The method is initializing life bitmap
    private void lifeBitmapInitialize() {
        this.listOfLifeBitmap = new ArrayList<>();
        this.listOfLifeBitmap.add(getScaledLifeBitmap(R.drawable.redheart));
        this.listOfLifeBitmap.add(getScaledLifeBitmap(R.drawable.redheart));
        this.listOfLifeBitmap.add(getScaledLifeBitmap(R.drawable.redheart));

        this.blackLife = getScaledLifeBitmap(R.drawable.blackheart).getLoveBitmap();
        this.redLife = getScaledLifeBitmap(R.drawable.redheart).getLoveBitmap();
    }

    //Create scaled bitmap for Life lIne
    private LifeBitmap getScaledLifeBitmap(int resouceID) {
        Bitmap bitmap = BitmapFactory.decodeResource(this.activity.getResources(), resouceID);
        bitmap = Bitmap.createScaledBitmap(bitmap, GameSurface.getScreenWidth() / 15, (GameSurface.getScreenHeight() / GameSurface.getScreenWidth()) * (GameSurface.getScreenWidth() / 15), true);
        return new LifeBitmap(bitmap);
    }

    //The method is about to setup
    //the sharedPreference to soter the high score in the
    private void setupGameHighScore() {
        SharedPreferences sharedPreferences = this.activity.getSharedPreferences(HIGH_SCORE_PREFERENCE, Context.MODE_PRIVATE);
        int highScore = sharedPreferences.getInt(HIGH_SCORE, 0);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(HIGH_SCORE, this.score);
        editor.commit();
    }

    //The method is about getting the bitmap in a scaled format
    public Bitmap scaledBallBitmap(Bitmap bitmap) {
        return Bitmap.createScaledBitmap(bitmap, GameSurface.getScreenWidth() / 7, (GameSurface.getScreenHeight() / GameSurface.getScreenWidth()) * (GameSurface.getScreenWidth() / 7), true);
    }


    //The method is about getting the bitmap in a scaled format
    public Bitmap scaledBitmap(Bitmap bitmap) {
        return Bitmap.createScaledBitmap(bitmap, GameSurface.getScreenWidth() / 6, (GameSurface.getScreenHeight() / GameSurface.getScreenWidth()) * (GameSurface.getScreenWidth() / 6), true);
    }

    //The method is about getting the bitmap in a scaled format
    public Bitmap getScaledObastacleBitmap(int id) {

        Bitmap obstacleBitMap = BitmapFactory.decodeResource(context.getResources(), id);
       obstacleBitMap = Bitmap.createScaledBitmap(obstacleBitMap, GameSurface.getScreenWidth() / 6, GameSurface.getScreenHeight() / 50, true);
      // obstacleBitMap = Bitmap.createScaledBitmap(obstacleBitMap, GameSurface.getScreenWidth() / 5, GameSurface.getScreenHeight() / 5, true);
        return obstacleBitMap;
    }

    public void setGameBall(Ball ball) {
        this.ball = ball;
    }

    public Bitmap getBallBitmap() {
        return ballBitmap;
    }



    //Update Canvas
    private void updateCanvas() {
        //Drawing the back groung of the game
        canvas.drawARGB(255, 40, 40, 48);

        //Ball is Showed
        if (GameSurface.isBallShowed()) {

            //Drawing the ball on the canvas
            canvas.drawBitmap(ball.getBallBitMap(), ball.getCenterX(), ball.getCenterY(), null);


            TextPaint paint = new TextPaint(TextPaint.ANTI_ALIAS_FLAG);
            paint.setColor(Color.rgb(231, 231, 231));
            paint.setTextSize(GameSurface.getScreenHeight() / 25);


            //Draw left side Obstacle on the canvas
            for (int i = 0; i < this.getLeftSideObstacle().size(); i++) {



                Obstacle obstacle = this.getLeftSideObstacle().get(i);
                drawLeftSideObstacle(obstacle);

                if (isCollisionDetected(obstacle.getBitmap(), (int) obstacle.getCenterX(), (int) obstacle.getCenterY(), ball.getBallBitMap(), (int) ball.getCenterX(), (int) ball.getCenterY())){

                    getLeftSideObstacle().remove(obstacle);
                    blinkCanvas();
                    gameOverAndRestartCanvas(paint);
                }

                Log.d("Life",this.lifeLine+"");

            }

            //Right Side obstable
            //TODO
            for (int i = 0; i < this.getRightSideObstacle().size(); i++) {


                Obstacle obstacle = this.getRightSideObstacle().get(i);
                drawRightSideObstacle(obstacle);

                if (isCollisionDetected(obstacle.getBitmap(), (int) obstacle.getCenterX(), (int) obstacle.getCenterY(), ball.getBallBitMap(), (int) ball.getCenterX(), (int) ball.getCenterY())){

                    getRightSideObstacle().remove(obstacle);
                    blinkCanvas();
                    gameOverAndRestartCanvas(paint);
                }



                Log.d("Life",this.lifeLine+"");

            }

            //bonusLife();
            //Draw the life line
            //and score
             drawLifeLineAndScore(paint);

        } else {
            //Default Message At first Start of the Game
            TextPaint paint = new TextPaint(TextPaint.ANTI_ALIAS_FLAG);
            paint.setColor(Color.WHITE);
            paint.setTextSize(GameSurface.getScreenHeight() / 30);
            //Draw the High score before start the game
            //Drawing the High Score on the canvas
            TextPaint paints = new TextPaint(TextPaint.ANTI_ALIAS_FLAG);
            paints.setColor(Color.rgb(253, 91, 3));
            paints.setTextSize(GameSurface.getScreenHeight() / 10);

            SharedPreferences sharedPreferences = this.activity.getSharedPreferences(HIGH_SCORE_PREFERENCE, Context.MODE_PRIVATE);
            int highScore = sharedPreferences.getInt(HIGH_SCORE, 0);
            canvas.drawText("High Score ", GameSurface.getScreenWidth() / 20, GameSurface.getScreenHeight() / 3, paints);
            canvas.drawText("" + highScore, GameSurface.getScreenWidth() / 2.8f, GameSurface.getScreenHeight() / 3 + paints.getTextSize(), paints);
            //Start Message
            canvas.drawText("Touch On The Screen", GameSurface.getScreenWidth() / 5, GameSurface.getScreenHeight() / 2, paint);
            canvas.drawText("To Start The Game", varying + GameSurface.getScreenWidth() / 5, paint.getTextSize() + GameSurface.getScreenHeight() / 2, paint);
        }
    }

    //Method
    //Blink Canvas when clash happend
    //with ball and obstacle
    private void blinkCanvas() {
        canvas.drawARGB(180, 128, 128, 128);
        canvas.drawARGB(180, 148, 148, 152);
        canvas.drawARGB(180, 201, 201, 203);
    }

    //The method is about
    //Drawing Life line and Score
    private void drawLifeLineAndScore(TextPaint paint) {
        int tolarence = 0;
        LifeBitmap lifeBitmap = null;
        for (int i = 0; i < this.getListOfLifeBitmap().size(); i++) {
            lifeBitmap = this.getListOfLifeBitmap().get(i);

            canvas.drawBitmap(lifeBitmap.getLoveBitmap(), GameSurface.getScreenWidth() / 20 + tolarence, lifeBitmap.getLoveBitmap().getHeight() / 4, null);
            tolarence += lifeBitmap.getLoveBitmap().getWidth() + 10;
        }

        //Print Score
        canvas.drawText("Your Score: " + this.getScore(), tolarence + lifeBitmap.getLoveBitmap().getWidth(), GameSurface.getScreenHeight() / 25, paint);

    }

    //The method is about to draw the Right Side obstacle
    private void drawRightSideObstacle(Obstacle obstacle) {
        canvas.drawBitmap(obstacle.getBitmap(), obstacle.getCenterX(), obstacle.getCenterY(), null);
    }

    //This method is handle the life
    //of the game and draw game over restart button
    //according the game situation
    private void gameOverAndRestartCanvas(TextPaint paint) {
        this.lifeLine -= 1;
        this.getListOfLifeBitmap().get(indexOfLife).setLoveBitmap(blackLife);
        this.indexOfLife += 1;

        if (this.lifeLine == 0) {
            this.stopThread();

            //Game Over Message Print
            TextPaint paints = new TextPaint(TextPaint.ANTI_ALIAS_FLAG);
            paints.setColor(Color.rgb(227, 0, 14));
            paints.setTextSize(GameSurface.getScreenHeight() / 10);
            canvas.drawText("Game Over", GameSurface.getScreenWidth() / 15, GameSurface.getScreenHeight() / 2.5f - paint.getTextSize(), paints);

            //Draw restart button
            canvas.drawBitmap(this.restartButton.getBitmap(), this.restartButton.getCenterX(), this.restartButton.getCenterY(), null);

            //calling the High score method
            updateHighScoreAndShow();
        }


    }

    //The method is about to draw the leftside obstacle
    private void drawLeftSideObstacle(Obstacle obstacle) {
        canvas.drawBitmap(obstacle.getBitmap(), obstacle.getCenterX(), obstacle.getCenterY(), null);
    }

    //This method is about to draw the accelerometer
    //value in the canvas
    private void drawAcceleroMeter() {
        Paint paint = new Paint();
        paint.setColor(Color.BLUE);
        paint.setTextSize(GameSurface.getScreenWidth() / 10);
        canvas.drawText("X- Axis: " + GameActivity.getGravityX(), 10, GameSurface.getScreenHeight() / 2, paint);
        canvas.drawText("Y- Axis: " + GameActivity.getGravityY(), 10, GameSurface.getScreenHeight() / 2 + paint.getTextSize(), paint);
    }

    //Stop Thread
    public void stopThread() {
        this.threadFlag = false;
    }

    //Ball

    public Ball getBall() {
        return ball;
    }

    //This method is about the store and show the the
    //High score in the canvas
    public void updateHighScoreAndShow() {
        SharedPreferences sharedPreferences = this.activity.getSharedPreferences(HIGH_SCORE_PREFERENCE, Context.MODE_PRIVATE);
        int highScore = sharedPreferences.getInt(HIGH_SCORE, 0);

        //The condition is to check the current score is
        // greater than the high or not
        if (this.score > highScore) {
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putInt(HIGH_SCORE, this.score);
            editor.commit();

            //Drawing the High Score on the canvas
            TextPaint paints = new TextPaint(TextPaint.ANTI_ALIAS_FLAG);
            paints.setColor(Color.rgb(239, 244, 228));
            paints.setTextSize(GameSurface.getScreenHeight() / 10);
            canvas.drawText("High Score ", GameSurface.getScreenWidth() / 20, GameSurface.getScreenHeight() / 10 + GameSurface.getScreenHeight() / 2.5f, paints);
            canvas.drawText("" + this.score, GameSurface.getScreenWidth() / 2.5f, GameSurface.getScreenHeight() / 10 + paints.getTextSize() + GameSurface.getScreenHeight() / 2.5f, paints);
            Log.d("highscore", this.score + "");
        }
    }


    @Override
    public boolean isCollisionDetected(Bitmap bitmap1, int x1, int y1, Bitmap bitmap2, int x2, int y2) {
        Rect bounds1 = new Rect(x1, y1, x1 + bitmap1.getWidth(), y1 + bitmap1.getHeight());
        Rect bounds2 = new Rect(x2, y2, x2 + bitmap2.getWidth(), y2 + bitmap2.getHeight());

        if (Rect.intersects(bounds1, bounds2)) {
            Rect collisionBounds = getCollisionBounds(bounds1, bounds2);
            for (int i = collisionBounds.left; i < collisionBounds.right; i++) {
                for (int j = collisionBounds.top; j < collisionBounds.bottom; j++) {
                    int bitmap1Pixel = bitmap1.getPixel(i - x1, j - y1);
                    int bitmap2Pixel = bitmap2.getPixel(i - x2, j - y2);
                    if (isFilled(bitmap1Pixel) && isFilled(bitmap2Pixel)) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    @Override
    public Rect getCollisionBounds(Rect rect1, Rect rect2) {
        int left = (int) Math.max(rect1.left, rect2.left);
        int top = (int) Math.max(rect1.top, rect2.top);
        int right = (int) Math.min(rect1.right, rect2.right);
        int bottom = (int) Math.min(rect1.bottom, rect2.bottom);
        return new Rect(left, top, right, bottom);
    }

    @Override
    public boolean isFilled(int pixel) {
        return pixel != Color.TRANSPARENT;
    }

    public  int getScore() {
        return score;
    }

    public  void setScore(int score) {
        this.score = score;
    }

    public   void increamentScore(int sc) {
        Log.d("Running",this.score+" ");
        this.score += sc;
    }

    public  Bitmap getRestartBitmap() {
        return restartBitmap;
    }

    public  ArrayList<Bitmap> getListOfObstableBitmap() {
        return listOfObstableBitmap;
    }

    public  ArrayList<Obstacle> getLeftSideObstacle() {
        return leftSideObstacle;
    }

    public  ArrayList<LifeBitmap> getListOfLifeBitmap() {
        return listOfLifeBitmap;
    }

    public  RestartBitmap getRestartButton() {
        return restartButton;
    }

    public   ArrayList<Obstacle> getRightSideObstacle(){
        return this.rightSideObstacle;
    }


    public  void bonusLife(int gameScore){
        //Todo
        if (gameScore % 500 == 0 && (this.lifeLine > 0 && this.lifeLine < 3)) {
            this.lifeLine += 1;
            this.indexOfLife -= 1;
            this.getListOfLifeBitmap().get(indexOfLife).setLoveBitmap(redLife);
        }

    }

    public  int getLifeLine() {
        return lifeLine;
    }

    public  void setLifeLine(int lifeLine) {
        this.lifeLine = lifeLine;
    }

    public  int getIndexOfLife() {
        return indexOfLife;
    }

    public  void setIndexOfLife(int indexOfLife) {
        this.indexOfLife = indexOfLife;
    }
}
