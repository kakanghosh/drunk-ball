package com.example.ghosh.drunkball.animation;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.example.ghosh.drunkball.bitmapDesign.Ball;
import com.example.ghosh.drunkball.gameLoop.GameActivity;
import com.example.ghosh.drunkball.gameLoop.GameSurface;
import com.example.ghosh.drunkball.gameLoop.GameThread;
import com.example.ghosh.drunkball.settings.GameSetting;

/**
 * Created by GHOSH on 5/8/2017.
 */

public class GameAnimation extends Thread {
    private boolean threadFlag;
    private final int MAX_FPS = 60;
    private GameThread gameThread;
    private float time = 0.3f;
    private float retarTime = -0.7f;
    private float gX ,gY;
    private float left, top, right, bottom;
    private GameActivity activity;
    private int ballSpeed;
    public GameAnimation(GameThread gameThread,GameActivity activity){
        this.gameThread = gameThread;
        this.activity = activity;
        initializeScreenSize();
    }

    private void initializeScreenSize() {
        left = 0;
        top = 0;
        right = GameSurface.getScreenWidth() - gameThread.getBallBitmap().getWidth();
        bottom = GameSurface.getScreenHeight() - gameThread.getBallBitmap().getHeight();

        //Initialize ball Speed
        SharedPreferences shared = activity.getSharedPreferences("GAME_SPEED",Context.MODE_PRIVATE);
        ballSpeed = shared.getInt(GameSetting.BALL_SPEED,0);
    }

    @Override
    public void run() {
        this.threadFlag = true;
        long startTime;
        long targetTime = 1000 / MAX_FPS;
        long waitingTime;

        while (this.threadFlag){
            startTime = System.nanoTime();
            this.gX = GameActivity.getGravityX();
            this.gY = GameActivity.getGravityY();
            //Set the speed of the ball on the game level
            setGravityOnBallSpeed();
            updateBallAnimation();
            waitingTime = targetTime - ( (System.nanoTime() - startTime) / 1000000 );
            if (waitingTime > 0){
                try {
                    this.sleep(waitingTime);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }

    }

    //The method is about to determined the
    //ball speed in the basis on the game level select
    private void setGravityOnBallSpeed() {
        Log.d("Speed",ballSpeed+"");
        switch (ballSpeed){
            case 0:
                this.gX *= .5f;
                this.gY *= .5f;
                break;
            case 1:
                this.gX *= .7f;
                this.gY *= .7f;
                break;
            case 2:
                this.gX *= 1.1f;
                this.gY *= 1.1f;
                break;
            case 3:
                this.gX *= 1.3f;
                this.gY *= 1.3f;
                break;
        }
    }

    private void updateBallAnimation() {


        if (GameSurface.isBallShowed()) {
            Ball ball = gameThread.getBall();
            if (ball != null){

               ball.increamentCenterX(ball.getVelocityX()*time + 0.5f * this.gX * time * time);
               ball.increamentVelocityX(this.gX * time);

               ball.increamentCenterY(ball.getVelocityY()*time + 0.5f * this.gY * time * time);
               ball.increamentVelocityY(this.gY * time);

               if (ball.getCenterX() < left){
                   ball.setCenterX(left);
                   ball.setVelocityX(ball.getVelocityX()*retarTime);
               }else  if (ball.getCenterX() > right){
                   ball.setCenterX(right);
                   ball.setVelocityX(ball.getVelocityX()*retarTime);
               }

               if (ball.getCenterY() < top){
                   ball.setCenterY(top);
                   ball.setVelocityY(ball.getVelocityY()*retarTime);
               }else  if (ball.getCenterY() > bottom){
                   ball.setCenterY(bottom);
                   ball.setVelocityY(ball.getVelocityY()*retarTime);
               }
            }
        }


    }

    public void stopAnimationTthread(){
        this.threadFlag = false;
    }
}
