package com.example.ghosh.drunkball.gameLoop;

import android.app.Activity;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.example.ghosh.drunkball.R;
import com.example.ghosh.drunkball.StartActivity;
import com.example.ghosh.drunkball.windowFeature.WindowFeature;

public class GameActivity extends Activity {
    private SensorManager sensorManager;
    private SensorEventListener sensorEventListener;
    private Sensor accelerometerSensor;
    private static float gravityX;
    private static float gravityY;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        new WindowFeature(this);
        initializeSensors();
        setContentView(new GameSurface(this,this));
    }

    private void initializeSensors() {
        this.sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        this.sensorEventListener = new SensorEventListener() {
            @Override
            public void onSensorChanged(SensorEvent event) {
                if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER){
                    gravityX = -event.values[0];
                    gravityY = event.values[1];
                }
            }

            @Override
            public void onAccuracyChanged(Sensor sensor, int accuracy) {

            }
        };
        this.accelerometerSensor = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        startUsingSensor();
    }

    private void startUsingSensor() {
        sensorManager.registerListener(sensorEventListener,accelerometerSensor,SensorManager.SENSOR_DELAY_NORMAL);
    }

    @Override
    protected void onPause() {
        super.onPause();
        stopUsingSensor();
    }

    private void stopUsingSensor() {
        sensorManager.unregisterListener(sensorEventListener);
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        startUsingSensor();
    }

    @Override
    protected void onStop() {
        super.onStop();
        stopUsingSensor();
    }

    public static float getGravityX() {
        return gravityX;
    }

    public static float getGravityY() {
        return gravityY;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Log.d("Created","Finish Activity");
        Intent intent = new Intent(this, StartActivity.class);
        startActivity(intent);
        this.finish();
    }
}
