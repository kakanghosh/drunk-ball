package com.example.ghosh.drunkball.gameInterface;

import android.graphics.Bitmap;
import android.graphics.Rect;

/**
 * Created by GHOSH on 5/8/2017.
 */

public interface GameCollision {
    /**
     * @param bitmap1 First bitmap
     * @param x1 x-position of bitmap1 on screen.
     * @param y1 y-position of bitmap1 on screen.
     * @param bitmap2 Second bitmap.
     * @param x2 x-position of bitmap2 on screen.
     * @param y2 y-position of bitmap2 on screen.
     */
    boolean isCollisionDetected(Bitmap bitmap1, int x1, int y1,
                                        Bitmap bitmap2, int x2, int y2);
    Rect getCollisionBounds(Rect rect1, Rect rect2);
    boolean isFilled(int pixel);
}
