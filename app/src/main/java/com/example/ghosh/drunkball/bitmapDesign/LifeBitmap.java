package com.example.ghosh.drunkball.bitmapDesign;

import android.graphics.Bitmap;

/**
 * Created by GHOSH on 5/10/2017.
 */

public class LifeBitmap {
    Bitmap loveBitmap;
    float centerX, centerY;

    public LifeBitmap(Bitmap loveBitmap) {
        this.loveBitmap = loveBitmap;
        this.centerX = this.centerY = 0;
    }

    public  LifeBitmap(Bitmap loveBitmap, float centerX, float centerY) {
        this.loveBitmap = loveBitmap;
        this.centerX = centerX;
        this.centerY = centerY;
    }

    public  Bitmap getLoveBitmap() {
        return loveBitmap;
    }

    public  void setLoveBitmap(Bitmap loveBitmap) {
        this.loveBitmap = loveBitmap;
    }

    public  float getCenterX() {
        return centerX;
    }

    public  void setCenterX(float centerX) {
        this.centerX = centerX;
    }

    public  float getCenterY() {
        return centerY;
    }

    public  void setCenterY(float centerY) {
        this.centerY = centerY;
    }


}
