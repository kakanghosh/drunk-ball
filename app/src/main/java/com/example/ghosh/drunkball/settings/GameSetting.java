package com.example.ghosh.drunkball.settings;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.SeekBar;
import android.widget.TextView;

import com.example.ghosh.drunkball.R;
import com.example.ghosh.drunkball.windowFeature.WindowFeature;

public class GameSetting extends Activity implements SeekBar.OnSeekBarChangeListener{
    private SeekBar gameSpeedSeekBar;
    private TextView gameSpeedTextView;
    private int ballSpeed = 0;
    public static String BALL_SPEED = "_ball_speed_";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        new WindowFeature(this);
        setContentView(R.layout.activity_game_setting);
        initializeAll();
    }

    private void initializeAll() {
        this.gameSpeedSeekBar = (SeekBar) findViewById(R.id.gameSpeedSeekBar);
        this.gameSpeedTextView = (TextView) findViewById(R.id.ballSpeedText);

        //Initialize ball Speed
        SharedPreferences shared = getSharedPreferences("GAME_SPEED",Context.MODE_PRIVATE);
        ballSpeed = shared.getInt(GameSetting.BALL_SPEED,0);
        Log.d("Speed",ballSpeed+"");
        this.gameSpeedSeekBar.setProgress(ballSpeed);
        setGameSpeedTitle(ballSpeed);

        this.gameSpeedSeekBar.setOnSeekBarChangeListener(this);
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        switch (progress){
            case 0:
                gameSpeedTextView.setText("Ball Speed: KID");
                ballSpeed = 0;
                break;
            case 1:
                gameSpeedTextView.setText("Ball Speed: MAN");
                ballSpeed = 1;
                break;
            case 2:
                gameSpeedTextView.setText("Ball Speed: ALIEN");
                ballSpeed = 2;
                break;
            case 3:
                gameSpeedTextView.setText("Ball Speed: LEGEND");
                ballSpeed = 3;
                break;
        }

        SharedPreferences sharedPreferences = getSharedPreferences("GAME_SPEED",Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(BALL_SPEED,ballSpeed);
        editor.commit();


    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {

    }

    private void setGameSpeedTitle(int n){

        switch (n){
            case 0:
                gameSpeedTextView.setText("Ball Speed: KID");
                break;
            case 1:
                gameSpeedTextView.setText("Ball Speed: MAN");
                break;
            case 2:
                gameSpeedTextView.setText("Ball Speed: ALIEN");
                break;
            case 3:
                gameSpeedTextView.setText("Ball Speed: LEGEND");
                break;
        }
    }

}
