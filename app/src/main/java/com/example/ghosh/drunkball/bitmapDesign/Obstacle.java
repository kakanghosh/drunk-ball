package com.example.ghosh.drunkball.bitmapDesign;

import android.graphics.Bitmap;
import android.graphics.Point;
import android.graphics.Rect;

/**
 * Created by GHOSH on 5/8/2017.
 */

public class Obstacle {
    Bitmap bitmap;
    float centerX;
    float centerY;

    public Obstacle(Bitmap bitmap, Point point){
        this.bitmap = bitmap;
        setPoint(point);
    }

    public  void setPoint(Point point){
        this.centerX = point.x;
        this.centerY = point.y;
    }

    public  Bitmap getBitmap() {
        return bitmap;
    }

    public  void setBitmap(Bitmap bitmap) {
        this.bitmap = bitmap;
    }

    public  float getCenterX() {
        return centerX;
    }

    public  void setCenterX(float centerX) {
        this.centerX = centerX;
    }

    public  float getCenterY() {
        return centerY;
    }

    public  void setCenterY(float centerY) {
        this.centerY = centerY;
    }

    public  void decrementCenterX(int value){
        this.centerX -= value;
    }

    public  void incrementCenterX(int value){
        this.centerX += value;
    }

    public  void incrementCenterY(int value){
        this.centerY += value;
    }

    public  void decrementCenterY(int value){
        this.centerY -= value;
    }
}

