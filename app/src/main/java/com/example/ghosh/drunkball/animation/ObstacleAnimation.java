package com.example.ghosh.drunkball.animation;

import android.graphics.Point;
import android.util.Log;

import com.example.ghosh.drunkball.gameLoop.GameActivity;
import com.example.ghosh.drunkball.gameLoop.GameSurface;
import com.example.ghosh.drunkball.gameLoop.GameThread;
import com.example.ghosh.drunkball.bitmapDesign.Obstacle;

import java.util.Random;

/**
 * Created by GHOSH on 5/8/2017.
 */

public class ObstacleAnimation extends Thread {
    private boolean threadFlag;
    private final int MAX_FPS = 60;
    private GameThread gameThread;
    private int leftStartingPoint;
    private int rightStartingPoint;
    private int time1 = 3000;
    private int time2 = 3500;

    private int time = 3000;
    private int scoreTime = 400;

    public ObstacleAnimation(GameThread gameThread){
        this.gameThread = gameThread;
        leftStartingPoint  = -gameThread.getListOfObstableBitmap().get(0).getWidth();
        rightStartingPoint = gameThread.getListOfObstableBitmap().get(0).getWidth() + GameSurface.getScreenWidth();
    }

    @Override
    public void run() {
        this.threadFlag = true;
        long startTime;
        long targetTime = 1000 / MAX_FPS;
        long waitingTime;
        long totalTime = 0;
        long scoreTotalTime = 0;
        long rightSideObstacleTime = 0;

        //starting score
        int startingScore = 50 + new Random().nextInt(50);

        //////////////////

        while (this.threadFlag){



            if (GameSurface.isBallShowed()) {
                startTime = System.nanoTime();
                waitingTime = targetTime - ( (System.nanoTime() - startTime) / 1000000 );
                animateObstacle();
                if (waitingTime > 0){
                    try {
                        this.sleep(waitingTime);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                totalTime += (System.nanoTime() - startTime)/1000000;
                rightSideObstacleTime += (System.nanoTime() - startTime)/1000000;
                scoreTotalTime += (System.nanoTime() - startTime)/1000000;

                //Add left side obstacle in the game
                if (totalTime >= time1){
                    Log.d("Ready",totalTime+"");
                    addLeftObstacle();
                    totalTime = 0;

                }


                //Add right side obstacle in the game
                if (rightSideObstacleTime >= time2) {
                    if (gameThread.getScore() > startingScore){
                        addRightSideObstacle();
                    }

                    Log.d("Obstacle","Right SIde Obstacle "+rightSideObstacleTime);
                    rightSideObstacleTime = 0;
                }

                //Score increament and all thing about score
                if (scoreTotalTime >= scoreTime){
                    //increament score
                    this.gameThread.increamentScore(1);
                   // this.gameThread.bonusLife(this.gameThread.getScore());

                    if (this.gameThread.getScore() % 10 == 0){
                        Log.d("Time",this.time+"");
                        if (time1 > 500) {
                            time1 -= 35;
                        }
                    }
                     scoreTotalTime = 0;
                }


            }

        }

    }

    private void addRightSideObstacle() {
        int yPosition = (int) gameThread.getBall().getCenterY();
        Obstacle obstacle = new Obstacle( gameThread.getListOfObstableBitmap().get( new Random().nextInt(5)),new Point(rightStartingPoint,yPosition));
        gameThread.getRightSideObstacle().add(obstacle);
    }

    private void addLeftObstacle() {
//        if (gameThread.getBall() == null)
//            return;
        //Get Y position of the game ball
        int yPosition = (int) gameThread.getBall().getCenterY();
        Obstacle obstacle = new Obstacle( gameThread.getListOfObstableBitmap().get( new Random().nextInt(5)),new Point(leftStartingPoint,yPosition));
        gameThread.getLeftSideObstacle().add(obstacle);
    }

    private void animateObstacle() {

     //   if (GameSurface.isBallShowed()) {
        //Left side obstacle
            for (int i = 0; i < gameThread.getLeftSideObstacle().size(); i++){
                Obstacle obstacle = gameThread.getLeftSideObstacle().get(i);
                if (obstacle.getCenterX() > GameSurface.getScreenWidth()+obstacle.getBitmap().getWidth()){
                    gameThread.getLeftSideObstacle().remove(obstacle);
                }else {
                    obstacle.incrementCenterX(2);
                }
            }
        //Right side obstacle
        for (int i = 0; i < gameThread.getRightSideObstacle().size(); i++){
            Obstacle obstacle = gameThread.getRightSideObstacle().get(i);
            if (obstacle.getCenterX() < -obstacle.getBitmap().getWidth()){
                gameThread.getRightSideObstacle().remove(obstacle);
            }else {
                obstacle.decrementCenterX(2);
            }
        }
     //   }

    }

    public void stopObstacleAnimation(){
        this.threadFlag = false;
    }

    public void decrementGameTime(int n){
        this.time1 -= n;
    }

    public int getTime(){
        return this.time1;
    }
}
