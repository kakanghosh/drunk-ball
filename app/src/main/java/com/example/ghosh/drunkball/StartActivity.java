package com.example.ghosh.drunkball;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.example.ghosh.drunkball.gameLoop.GameActivity;
import com.example.ghosh.drunkball.settings.GameSetting;
import com.example.ghosh.drunkball.settings.HowToPlay;
import com.example.ghosh.drunkball.windowFeature.WindowFeature;

public class StartActivity extends Activity implements View.OnClickListener {
    private Button playButton;
    private Button optionButton;
    private Button settingsButton;
    private Button exitButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Modify Title and set flag for screen
        new WindowFeature(this);
        setContentView(R.layout.activity_start);
        initializeButton();
    }

    private void initializeButton() {

        this.playButton = (Button) findViewById(R.id.buttonPlay);
        this.optionButton = (Button) findViewById(R.id.howToPlayButton);
        this.settingsButton = (Button) findViewById(R.id.settingButton);
        this.exitButton = (Button) findViewById(R.id.buttonExit);

        this.playButton.setOnClickListener(this);
        this.optionButton.setOnClickListener(this);
        this.settingsButton.setOnClickListener(this);
        this.exitButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.buttonPlay) {
            Intent intent = new Intent(this, GameActivity.class);
            startActivity(intent);
            this.finish();
        } else if (view.getId() == R.id.howToPlayButton) {
            Intent intent = new Intent(this, HowToPlay.class);
            startActivity(intent);
        } else if (view.getId() == R.id.settingButton) {
                Intent intent = new Intent(this, GameSetting.class);
                startActivity(intent);

        } else if (view.getId() == R.id.buttonExit) {
            this.finish();
        }
    }
}
