package com.example.ghosh.drunkball.settings;

import android.app.Activity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.ghosh.drunkball.R;
import com.example.ghosh.drunkball.windowFeature.WindowFeature;

public class HowToPlay extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        new WindowFeature(this);
        setContentView(R.layout.activity_how_to_play);
    }
}
