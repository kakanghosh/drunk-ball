package com.example.ghosh.drunkball.bitmapDesign;

import android.graphics.Bitmap;

/**
 * Created by GHOSH on 5/9/2017.
 */

public class RestartBitmap {
    Bitmap bitmap;
    float centerX,centerY;

    public RestartBitmap(Bitmap bitmap, float centerX, float centerY) {
        this.bitmap = bitmap;
        this.centerX = centerX;
        this.centerY = centerY;
    }

    public Bitmap getBitmap() {
        return bitmap;
    }

    public void setBitmap(Bitmap bitmap) {
        this.bitmap = bitmap;
    }

    public float getCenterX() {
        return centerX;
    }

    public void setCenterX(float centerX) {
        this.centerX = centerX;
    }

    public float getCenterY() {
        return centerY;
    }

    public void setCenterY(float centerY) {
        this.centerY = centerY;
    }
}
