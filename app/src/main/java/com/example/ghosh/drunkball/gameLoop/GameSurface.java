package com.example.ghosh.drunkball.gameLoop;

import android.content.Context;
import android.graphics.Point;
import android.util.Log;
import android.view.Display;
import android.view.MotionEvent;
import android.view.SurfaceView;
import android.view.SurfaceHolder;
import android.view.WindowManager;

import com.example.ghosh.drunkball.bitmapDesign.Ball;

/**
 * Created by GHOSH on 5/7/2017.
 */

public class GameSurface extends SurfaceView implements SurfaceHolder.Callback {
    private Context context;
    private static int SCREEN_WIDTH;
    private static int SCREEN_HEIGHT;
    private GameThread gameThread;
    private static boolean isBallShowed;
    private GameActivity activity;
    public GameSurface(Context context, GameActivity activity) {
        super(context);
        this.context = context;
        this.activity = activity;
        getHolder().addCallback(this);
        displayInfo();

    }

    //Get screen display size info
    private void displayInfo() {
        WindowManager windowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = windowManager.getDefaultDisplay();
        Point point = new Point();
        display.getSize(point);
        SCREEN_WIDTH = point.x;
        SCREEN_HEIGHT = point.y;
        Log.d("ScreenSize",SCREEN_WIDTH+"x"+SCREEN_HEIGHT);
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        startTheGame();
        Log.d("Created","Created");
    }

    public void startTheGame(){
        try {

            isBallShowed = false;
            this.gameThread = new GameThread(this.getHolder(),this.context,this.activity);
            this.gameThread.start();

        } catch (Exception e) {
            stopThreadForcefully();
            isBallShowed = false;
            this.gameThread = null;
            this.gameThread = new GameThread(this.getHolder(),this.context,this.activity);
            this.gameThread.start();
        }
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        stopThreadForcefully();
        Log.d("Created","Destroy");
    }

    //stop thread
    void stopThreadForcefully(){
        boolean retry = true;
        while (retry){
            try {
                this.gameThread.stopThread();

            } catch (Exception e) {
                e.printStackTrace();
            }
            retry = false;
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        Point point = new Point((int)event.getX(),(int)event.getY());
        switch (event.getAction()){

            case MotionEvent.ACTION_DOWN:
                if (!isBallShowed){
                    this.gameThread.setGameBall(new Ball(gameThread.getBallBitmap(),point));
                    this.isBallShowed = true;
                }

                if( point.x > gameThread.getRestartButton().getCenterX() &&
                        point.x < gameThread.getRestartButton().getCenterX() + gameThread.getRestartButton().getBitmap().getWidth() &&
                        point.y > gameThread.getRestartButton().getCenterY() &&
                        point.y < gameThread.getRestartButton().getCenterY() + gameThread.getRestartButton().getBitmap().getHeight() ) {
                    startTheGame();
                    Log.d("Restart","Restart Button Clicked");
                }

                break;
        }
        return true;
    }

    public static int getScreenWidth() {
        return SCREEN_WIDTH;
    }

    public static int getScreenHeight() {
        return SCREEN_HEIGHT;
    }

    public static  boolean isBallShowed() {
        return isBallShowed;
    }



}
