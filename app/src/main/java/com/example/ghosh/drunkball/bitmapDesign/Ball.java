package com.example.ghosh.drunkball.bitmapDesign;

import android.graphics.Bitmap;
import android.graphics.Point;
import android.util.Log;

import com.example.ghosh.drunkball.gameInterface.GameCollision;

/**
 * Created by GHOSH on 5/7/2017.
 */

public class Ball{
    Bitmap ballBitMap;
    float  centerX;
    float centerY;
    float velocityX;
    float velocityY;

    public Ball(Bitmap ballBitMap) {
        this.ballBitMap = ballBitMap;
        this.centerX = this.centerY = 0;
        this.velocityX = this.velocityY = 0;
    }

    public Ball(Bitmap ballBitMap, Point point){
        this(ballBitMap);
        setPoint(point);
    }

    public   void setPoint(Point point){
        this.centerX = point.x;
        this.centerY = point.y;
    }

    public   Bitmap getBallBitMap() {
        return ballBitMap;
    }

    public   void setBallBitMap(Bitmap ballBitMap) {
        this.ballBitMap = ballBitMap;
    }

    public  float getCenterX() {
        return centerX;
    }

    public  void setCenterX(float centerX) {
        this.centerX = centerX;
    }

    public  float getCenterY() {
        return centerY;
    }

    public  void setCenterY(float centerY) {
        this.centerY = centerY;
    }

    public  float getVelocityX() {
        return velocityX;
    }

    public  void setVelocityX(float velocityX) {
        this.velocityX = velocityX;
    }

    public  float getVelocityY() {
        return velocityY;
    }

    public  void setVelocityY(float velocityY) {
        this.velocityY = velocityY;
    }

    public  void increamentCenterX(float value){
        this.centerX += value;
    }

    public  void increamentCenterY(float value){
        this.centerY += value;
    }

    public  void increamentVelocityX(float value){
        this.velocityX += value;
    }

    public  void increamentVelocityY(float value){
        this.velocityY += value;
    }
}
